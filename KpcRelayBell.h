#ifndef KPCRELAYBELL_H_ZHMSX6WX
#define KPCRELAYBELL_H_ZHMSX6WX
#include <stdint.h>

/* Abstraction to control a bell attached to a relay. */

struct _KpcRelayBell;
typedef struct _KpcRelayBell KpcRelayBell;

/* NOTE:
 * Provide the implementaion specific init function in a separate header file.
 * See `KpcPeripherals.h` for explanation. */

void    kpc_relay_bell_on   (KpcRelayBell *bell);
void    kpc_relay_bell_off  (KpcRelayBell *bell);

#endif /* end of include guard: KPCRELAYBELL_H_ZHMSX6WX */
