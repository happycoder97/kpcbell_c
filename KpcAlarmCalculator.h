#ifndef KPCALARMCALCULATOR_H_SAL7Y4TP
#define KPCALARMCALCULATOR_H_SAL7Y4TP

#include "KpcTime.h"

/*  When current = NULL, returns first element.
 *  Returns NULL when no more elements are there. */
typedef const KpcAlarm * (*fn_alarms_iterator_next)(const KpcAlarm *current, void* iterator_data) ;

/* Given an iterator over alarms, returns which is the next closest alarm. */
const KpcAlarm *    kpc_alarm_calc_get_next_alarm(fn_alarms_iterator_next alarms, void* iterator_data, KpcTime time_now);


#endif /* end of include guard: KPCALARMCALCULATOR_H_SAL7Y4TP */
