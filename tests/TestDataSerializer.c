#include "FznTest.h"
#include "../KpcDataSerializer.h"
#include <stdio.h>
#include <string.h>

TEST(basic_requirements) {
    ASSERT_EQ((int)sizeof(KpcAlarmGroup), 16, "%u");
    ASSERT_EQ((int)sizeof(KpcAlarm), 5, "%u");
}

TEST(test_reset_should_invalidate_objects) {
    KpcDataSerializer data_ser;
    kpc_data_ser_reset_buffer(&data_ser);
    for(uint16_t i=0; i< KPC_DATA_SER_N_ALARMS; i++) {
        const KpcAlarm *alarm = kpc_data_ser_get_alarm(&data_ser, i);
        ASSERT_EQ_PTR(alarm, NULL);
    }
    for(uint16_t i=0; i< KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        const KpcAlarmGroup *alarm_group = kpc_data_ser_get_alarm_group(&data_ser, i);
        ASSERT_EQ_PTR(alarm_group, NULL);
    }
    for(uint16_t i=0; i< KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        bool is_enabled = kpc_data_ser_is_enabled_alarm_group(&data_ser, i);
        ASSERT_EQ_UINT(is_enabled, false);
    }
}

TEST(test_set_time_now) {
    KpcDataSerializer data_ser;
    KpcTime time = {12,12};
    kpc_data_ser_set_time_now(&data_ser, &time);
    KpcTime *time_ptr = (KpcTime *) (data_ser.buffer + _OFFSET_TIME_NOW);
    ASSERT_EQ_MEM(&time,time_ptr,sizeof(KpcTime));
}

TEST(test_set_next_alarm) {
    KpcDataSerializer data_ser;
    KpcAlarm alarm = {12,13};
    kpc_data_ser_set_next_alarm(&data_ser, &alarm);
    KpcAlarm *alarm_ptr = (KpcAlarm *)(data_ser.buffer + _OFFSET_NEXT_ALARM);
    ASSERT_EQ_MEM(&alarm,alarm_ptr, sizeof(KpcAlarm));
}

TEST(test_settings_flags) {
    KpcDataSerializer data_ser;
    kpc_data_ser_set_settings_flag(&data_ser,KPC_SETTINGS_IS_ALARM_ON);
    kpc_data_ser_clear_settings_flag(&data_ser,KPC_SETTINGS_IS_MP3_ON);
    ASSERT_EQ_INT(kpc_data_ser_is_settings_flag_set(&data_ser,KPC_SETTINGS_IS_ALARM_ON), 1);
    ASSERT_EQ_INT(kpc_data_ser_is_settings_flag_set(&data_ser,KPC_SETTINGS_IS_MP3_ON),   0);
}

TEST(test_set_warm_up_time) {
    KpcDataSerializer data_ser;
    kpc_data_ser_set_bell_warm_up_time(&data_ser, 100);
    ASSERT_EQ_INT(kpc_data_ser_get_bell_warm_up_time(&data_ser), 100);
}

TEST(test_set_security_pin) {
    KpcDataSerializer data_ser;
    kpc_data_ser_set_security_pin(&data_ser, 100);
    ASSERT_EQ_INT(kpc_data_ser_get_security_pin(&data_ser), 100);
}


TEST(test_set_alarm_works) {
    KpcDataSerializer data_ser;
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARMS; i++) {
        KpcAlarm alarm = {i*1, i*2, i*3, i*4, i*5};
        kpc_data_ser_set_alarm(&data_ser, i, &alarm);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARMS; i++) {
        const KpcAlarm *alarm = kpc_data_ser_get_alarm(&data_ser, i);
        KpcAlarm expected = {i*1, i*2, i*3, i*4, i*5};
        ASSERT_NE_PTR(alarm, NULL);
        ASSERT_EQ_MEM(alarm, &expected, sizeof(KpcAlarm));
    }
}

TEST(test_set_alarm_groups_works) {
    KpcDataSerializer data_ser;
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        KpcAlarmGroup alarm_group = {
            i*0xA, {i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD} };
        kpc_data_ser_set_alarm_group(&data_ser, i, &alarm_group);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        const KpcAlarmGroup *alarm_group = kpc_data_ser_get_alarm_group(&data_ser, i);
        KpcAlarmGroup expected = (KpcAlarmGroup){
            i*0xA, {i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD} };
        ASSERT_NE_PTR(alarm_group, NULL);
        ASSERT_EQ_MEM(alarm_group, &expected, sizeof(KpcAlarmGroup));
    }
}

TEST(test_alarm_group_is_disabled_on_create_after_buffer_reset) {
    KpcDataSerializer data_ser;
    kpc_data_ser_reset_buffer(&data_ser);
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        KpcAlarmGroup alarm_group;
        kpc_data_ser_set_alarm_group(&data_ser, i, &alarm_group);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        bool is_enabled = kpc_data_ser_is_enabled_alarm_group(&data_ser, i);
        ASSERT_EQ_UINT(is_enabled, false);
    }
}

TEST(test_del_alarm_works) {
    KpcDataSerializer data_ser;
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARMS; i++) {
        KpcAlarm alarm = {i*1, i*2, i*3, i*4, i*5};
        kpc_data_ser_set_alarm(&data_ser, i, &alarm);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARMS; i++) {
        kpc_data_ser_del_alarm(&data_ser, i);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARMS; i++) {
        const KpcAlarm *alarm = kpc_data_ser_get_alarm(&data_ser, i);
        ASSERT_EQ_PTR(alarm, NULL);
    }
}

TEST(test_del_alarm_group_works) {
    KpcDataSerializer data_ser;
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        KpcAlarmGroup alarm_group = {
            i*0xA, {i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD} };
        kpc_data_ser_set_alarm_group(&data_ser, i, &alarm_group);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        kpc_data_ser_del_alarm_group(&data_ser, i);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        const KpcAlarmGroup *alarm_group = kpc_data_ser_get_alarm_group(&data_ser, i);
        ASSERT_EQ_PTR(alarm_group, NULL);
    }
}

TEST(test_enable_disable_alarm_groups) {
    KpcDataSerializer data_ser;
    kpc_data_ser_reset_buffer(&data_ser);
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        KpcAlarmGroup alarm_group = {
            i*0xA, {i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD,
            i*0xA, i*0xB, i*0xC, i*0xD} };
        kpc_data_ser_set_alarm_group(&data_ser, i, &alarm_group);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        kpc_data_ser_del_alarm_group(&data_ser, i);
    }
    for(uint16_t i = 0; i<KPC_DATA_SER_N_ALARM_GROUPS; i++) {
        const KpcAlarmGroup *alarm_group = kpc_data_ser_get_alarm_group(&data_ser, i);
        ASSERT_EQ_PTR(alarm_group, NULL);
    }
}


int main(int argc,char **argv) {
    BEGIN_TESTS();
    CALL_TEST(basic_requirements);
    CALL_TEST(test_reset_should_invalidate_objects);
    CALL_TEST(test_set_alarm_works);
    CALL_TEST(test_del_alarm_works);
    CALL_TEST(test_set_alarm_groups_works);
    CALL_TEST(test_del_alarm_group_works);
    CALL_TEST(test_enable_disable_alarm_groups);
    CALL_TEST(test_alarm_group_is_disabled_on_create_after_buffer_reset);
    CALL_TEST(test_set_warm_up_time);
    CALL_TEST(test_set_security_pin);
    CALL_TEST(test_set_next_alarm);
    CALL_TEST(test_set_time_now);
    CALL_TEST(test_settings_flags);
    TEST_SUMMARY();
}
