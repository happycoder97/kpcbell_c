#ifndef FZNTEST_H_ULEXIGRU
#define FZNTEST_H_ULEXIGRU

/**
 * Minimal unit testing library
 */

#include <stdio.h>
#include <string.h>

#define TEST(test_name) static void test_name(int *__IS_TEST_SUCCESS)

#define __SET_TEST_FAILED() *__IS_TEST_SUCCESS = 0

#define ASSERT(expr) do {                           \
    if(!(expr)) {                                   \
        __SET_TEST_FAILED();                        \
        printf("Failed ASSERT("#expr"):\n");        \
        return;                                     \
    }                                               \
} while(0)                                          \

#define ASSERT_DETAILED(a, b, expr, fm_spec) do {   \
    if(!(expr)) {                                   \
        __SET_TEST_FAILED();                        \
        printf("    Failed ASSERT("#expr"):\n");    \
        printf("    "#a" = " fm_spec ";", (a));     \
        printf("\n");                               \
        printf("    "#b" = " fm_spec ";", (b));     \
        printf("\n");                               \
        return;                                     \
    }                                               \
} while (0)                                         \


#define ASSERT_EQ(a, b, format_spec) ASSERT_DETAILED ((a), (b), (a) == (b), format_spec)

#define ASSERT_EQ_INT(a, b)     ASSERT_EQ((a), (b), "%d")
#define ASSERT_EQ_UINT(a, b)    ASSERT_EQ((a), (b), "%u")
#define ASSERT_EQ_FLOAT(a, b)   ASSERT_EQ((a), (b), "%f")
#define ASSERT_EQ_PTR(a, b)     ASSERT_EQ((a), (b), "%p")
#define ASSERT_EQ_STR(a, b)     ASSERT_DETAILED((a), (b), strcmp((a), (b)), "%s")
#define ASSERT_EQ_MEM(a, b, len)     ASSERT_EQ_UINT(memcmp((a), (b), (len)), 0)

#define ASSERT_NE(a, b, format_spec) ASSERT_DETAILED ((a), (b), (a) != (b), format_spec)

#define ASSERT_NE_INT(a, b)     ASSERT_NE((a), (b), "%d")
#define ASSERT_NE_UINT(a, b)    ASSERT_NE((a), (b), "%u")
#define ASSERT_NE_FLOAT(a, b)   ASSERT_NE((a), (b), "%f")
#define ASSERT_NE_PTR(a, b)     ASSERT_NE((a), (b), "%p")
#define ASSERT_NE_STR(a, b)     ASSERT_DETAILED((a), (b), !strcmp((a), (b)), "%s")
#define ASSERT_NE_MEM(a, b, len)     ASSERT_NE_UINT(memcmp((a), (b), (len)), 0)

#define BEGIN_TESTS()                              \
    int __NUM_TESTS = 0;                           \
    int __TESTS_PASSED = 0;                        \

#define CALL_TEST(funcname) do {                   \
    int __IS_TEST_SUCCESS = 1;                     \
    funcname(&__IS_TEST_SUCCESS);                  \
    printf("-- "#funcname": ");                    \
    if(!__IS_TEST_SUCCESS)                         \
        printf("failed");                   	   \
    else                                           \
        printf("success");                         \
    printf("\n\n");                                \
    __TESTS_PASSED += __IS_TEST_SUCCESS;           \
    __NUM_TESTS += 1;                              \
} while(0)                                         \


#define TEST_SUMMARY() do {                        \
    printf("\nSUMMARY: %s\n",argv[0]);             \
    printf("    PASSED: %d \n", __TESTS_PASSED);     \
    printf("    FAILED: %d \n", __NUM_TESTS -        \
                              __TESTS_PASSED);     \
    printf("    TOTAL : %d \n", __NUM_TESTS);        \
} while(0)                                         \

#endif /* end of include guard: FZNTEST_H_ULEXIGRU */
