#include "FznTest.h"
#include "../KpcAlarmCalculator.h"

const KpcAlarm * alarms_array_iterator(const KpcAlarm * current, const KpcAlarm* array) {
    if(current == NULL) {
        return &(array[0]);
    }
    if(current - array >= 5) {
        return NULL;
    }
    return current+1;
}

TEST(next_alarm) {
    KpcAlarm alarms[5] = {
        {.hour_24 = 0, .minute = 0},
        {.hour_24 = 23,.minute = 59},
        {.hour_24 = 10,.minute = 59},
        {.hour_24 = 11,.minute = 0},
        {.hour_24 = 0, .minute = 1}
    };
    const KpcAlarm * next_alarm;
    KpcTime time_now;

    time_now = (KpcTime) {0,0};
    next_alarm = kpc_alarm_calc_get_next_alarm((fn_alarms_iterator_next) alarms_array_iterator, alarms, time_now);
    ASSERT_EQ(next_alarm-&alarms[4], 0L, "%lu");

    time_now = (KpcTime) {23,59};
    next_alarm = kpc_alarm_calc_get_next_alarm((fn_alarms_iterator_next) alarms_array_iterator, alarms, time_now);
    ASSERT_EQ(next_alarm-&alarms[0], 0L, "%lu");

    time_now = (KpcTime) {11,0};
    next_alarm = kpc_alarm_calc_get_next_alarm((fn_alarms_iterator_next) alarms_array_iterator, alarms, time_now);
    ASSERT_EQ(next_alarm - &alarms[1], 0L, "%lu");

    time_now = (KpcTime) {10,0};
    next_alarm = kpc_alarm_calc_get_next_alarm((fn_alarms_iterator_next) alarms_array_iterator, alarms, time_now);
    ASSERT_EQ(next_alarm - &alarms[2], 0L, "%lu");

    time_now = (KpcTime) {10,59};
    next_alarm = kpc_alarm_calc_get_next_alarm((fn_alarms_iterator_next) alarms_array_iterator, alarms, time_now);
    ASSERT_EQ(next_alarm - &alarms[3], 0L, "%lu");
}

int main(int argc, char ** argv) {
    BEGIN_TESTS();
    CALL_TEST(next_alarm);
    TEST_SUMMARY();
}
