#ifndef KPCHAL_H_PAGK3P6R
#define KPCHAL_H_PAGK3P6R

#include <stdint.h>

/* Milliseconds since the device has turned on. */
uint32_t    kpc_hal_millis();
uint32_t    kpc_hal_sleep_millis(uint32_t millis);

/* Write data to eeprom, overwriting its contents.
 * The data is written starting from eeprom address zero. */
void        kpc_hal_eeprom_write    (uint16_t offset, uint8_t *data, uint16_t len);

/* Reads data to the given array, starting from zeroth address
 * of eeprom */
void        kpc_hal_eeprom_read     (uint16_t offset, uint8_t *data, uint16_t len);

#endif /* end of include guard: KPCHAL_H_PAGK3P6R */
