#ifndef __RTC_H__
#define __RTC_H__
#include <stdint.h>
#include "KpcTime.h"

/* Abstraction for RTC Clocks */
struct _KpcClock;
typedef struct _KpcClock KpcClock;

/* NOTE:
 * Provide the implementaion specific init function in a separate header file.
 * See `KpcPeripherals.h` for explanation. */

uint8_t         kpc_clock_get_hours_24h     (KpcClock *clock);
uint8_t         kpc_clock_get_minutes       (KpcClock *clock);


#endif /* end of include guard: __RTC_H__ */
