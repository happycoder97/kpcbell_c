#include "KpcAlarmCalculator.h"
#include "stddef.h"

static uint16_t
get_minutes_till_alarm(KpcAlarm alarm, KpcTime time_now) {
    if (alarm.hour_24 < time_now.hour_24 ||
            (alarm.hour_24 == time_now.hour_24 && alarm.minute <= time_now.minute) ) {
        // if today's alarm is over, take into account next day's.
        alarm.hour_24 += 24;
    }
    return alarm.hour_24      * ((uint16_t) 60) + alarm.minute -
           time_now.hour_24   * ((uint16_t) 60) + time_now.minute;
}


const KpcAlarm *
kpc_alarm_calc_get_next_alarm(fn_alarms_iterator_next alarms_iterator_next, void* iterator_data, KpcTime time_now) {
    const KpcAlarm *current = NULL;
    const KpcAlarm *closest_alarm = NULL;
    uint16_t minutes_till_closest_alarm = UINT16_MAX;
    uint16_t minutes_till_current_alarm = UINT16_MAX;

    while( (current=alarms_iterator_next(current, iterator_data)) != NULL) {
        minutes_till_current_alarm = get_minutes_till_alarm(*current, time_now);
        if(minutes_till_current_alarm < minutes_till_closest_alarm) {
            closest_alarm = current;
            minutes_till_closest_alarm = minutes_till_current_alarm;
        }
    }
    return closest_alarm;
}

