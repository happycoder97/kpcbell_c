#ifndef KPCSOUNDPLAYER_H_7MNYQOKP
#define KPCSOUNDPLAYER_H_7MNYQOKP
#include <stdint.h>

/* Abstraction for playing sounds */
struct _KpcSoundPlayer;
typedef struct _KpcSoundPlayer KpcSoundPlayer;

/* NOTE:
 * Provide the implementaion specific init function in a separate header file.
 * See `KpcPeripherals.h` for explanation. */

uint8_t kpc_sound_player_get_n_sounds(KpcSoundPlayer *player);
void    kpc_sound_play_nth_sound(KpcSoundPlayer *player, uint8_t n);
void    kpc_sound_stop_playback(KpcSoundPlayer *player);

#endif /* end of include guard: KPCSOUNDPLAYER_H_7MNYQOKP */
