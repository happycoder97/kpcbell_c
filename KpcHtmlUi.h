#ifndef KPCHTMLUI_H_SNE78F3R
#define KPCHTMLUI_H_SNE78F3R

#include "KpcDataSerializer.h"

struct _KpcHtmlUi;
typedef struct _KpcHtmlUi KpcHtmlUi;

KpcHtmlUi *     kpc_html_ui_init(KpcDataSerializer *data_ser);

const char *    kpc_html_ui_get_index_html(KpcHtmlUi *html_ui);
const char *    kpc_get_styles_css(KpcHtmlUi *html_ui);

/* Iterate later as needed:
 *      * Add error message beside every form if submiting failed.
 */

#endif /* end of include guard: KPCHTMLUI_H_SNE78F3R */
