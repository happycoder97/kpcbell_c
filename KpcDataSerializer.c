#include "KpcDataSerializer.h"
#include "string.h"

/* index start from zero */
inline static uint16_t
_OFFSET_NTH_ALARM(uint8_t n) {
    return _OFFSET_ALARMS_DATA + n*sizeof(KpcAlarm);
}

/* index start from zero */
inline static uint16_t
_OFFSET_NTH_ALARM_GROUP(uint8_t n) {
    return _OFFSET_ALARM_GROUPS_DATA + n*sizeof(KpcAlarmGroup);
}

void
kpc_data_ser_reset_buffer (KpcDataSerializer *data_ser) {
    for(uint16_t i = 0; i < KPC_DATA_SER_BUFFER_LEN; ++i) {
        data_ser->buffer[i] = 0x00;
    }
}

void
kpc_data_ser_set_time_now (KpcDataSerializer *data_ser, KpcTime *time) {
    KpcTime *time_buffer = (KpcTime *)(data_ser->buffer+_OFFSET_TIME_NOW);
    *time_buffer = *time;
}

void
kpc_data_ser_set_next_alarm (KpcDataSerializer *data_ser, KpcAlarm *alarm) {
    KpcAlarm *alarm_buffer = (KpcAlarm *)(data_ser->buffer + _OFFSET_NEXT_ALARM);
    *alarm_buffer = *alarm;
}

void
kpc_data_ser_set_bell_warm_up_time (KpcDataSerializer *data_ser, uint8_t time_secs) {
    *((uint8_t *)(data_ser->buffer + _OFFSET_BELL_WARM_UP_TIME)) = time_secs;
}

void
kpc_data_ser_set_security_pin   (KpcDataSerializer *data_ser, uint16_t pin) {
    *((uint16_t *)(data_ser->buffer + _OFFSET_SECURITY_PIN)) = pin;
}

void
kpc_data_ser_set_settings_flag  (KpcDataSerializer *data_ser, uint8_t flag) {
    data_ser->buffer[_OFFSET_SETTINGS_FLAGS] |= flag;
}

void
kpc_data_ser_clear_settings_flag  (KpcDataSerializer *data_ser, uint8_t flag) {
    data_ser->buffer[_OFFSET_SETTINGS_FLAGS] &= ~flag;
}


uint8_t
kpc_data_ser_get_bell_warm_up_time      (KpcDataSerializer *data_ser) {
    return * (uint8_t *)(data_ser->buffer + _OFFSET_BELL_WARM_UP_TIME );
}

uint16_t
kpc_data_ser_get_security_pin      (KpcDataSerializer *data_ser) {
    return * (uint16_t *)(data_ser->buffer + _OFFSET_SECURITY_PIN );
}

uint8_t
kpc_data_ser_is_settings_flag_set  (KpcDataSerializer *data_ser, uint8_t flag) {
    return !(!(data_ser->buffer[_OFFSET_SETTINGS_FLAGS] & flag));
}

static void
set_bitfield (uint8_t *bit_field, uint8_t bit_index, bool value) {
    uint8_t  slot_index    = bit_index / 8;
    uint8_t *slot          = bit_field + slot_index;
    uint8_t  slot_subindex = bit_index % 8;
    uint8_t  mask          = 0b00000001 << slot_subindex;
    if(value) {
        *slot |=  mask;
    } else {
        *slot &= ~mask;
    }
}

static bool
get_bitfield (uint8_t *bit_field, uint8_t bit_index) {
    uint8_t  slot_index    = bit_index / 8;
    uint8_t *slot          = bit_field + slot_index;
    uint8_t  slot_subindex = bit_index % 8;
    uint8_t  mask          = 0b00000001 << slot_subindex;
    uint8_t  value         = !!( (*slot) & mask );
    return value;
}

static bool
is_alarm_valid (KpcDataSerializer *data_ser, uint8_t index) {
    return get_bitfield(data_ser->buffer+ _OFFSET_ALARMS_IS_VALID_BITFLD, index);
}
static void
set_alarm_valid (KpcDataSerializer *data_ser, uint8_t index, bool is_valid) {
    set_bitfield(data_ser->buffer+ _OFFSET_ALARMS_IS_VALID_BITFLD, index, is_valid);
}

static bool
is_alarm_group_valid (KpcDataSerializer *data_ser, uint8_t index) {
    return get_bitfield(data_ser->buffer+ _OFFSET_ALARM_GROUPS_IS_VALID_BITFLD, index);
}
static void
set_alarm_group_valid (KpcDataSerializer *data_ser, uint8_t index, bool is_valid) {
    set_bitfield(data_ser->buffer+ _OFFSET_ALARM_GROUPS_IS_VALID_BITFLD, index, is_valid);
}


/** MENTAL CONTEXT SAVE:
 *  Define static bool is_alarm_valid()
 *  Use it to return NUL when alarm is not set.
 *  Ditto for alarm groups
 *  Define is_enabled_alarm_group()
 */

const KpcAlarm *
kpc_data_ser_get_alarm (KpcDataSerializer *data_ser, uint8_t index) {
    if(!is_alarm_valid(data_ser, index)) {
        return 0;
    }
    return (const KpcAlarm *) (data_ser->buffer + _OFFSET_NTH_ALARM(index));
}

void
kpc_data_ser_set_alarm (KpcDataSerializer *data_ser, uint8_t index, const KpcAlarm * alarm) {
    memcpy(data_ser->buffer + _OFFSET_NTH_ALARM(index), alarm, sizeof(KpcAlarm));
    set_alarm_valid(data_ser, index, true);
}

void
kpc_data_ser_del_alarm (KpcDataSerializer *data_ser, uint8_t index) {
    set_alarm_valid(data_ser, index, false);
}

const KpcAlarmGroup *
kpc_data_ser_get_alarm_group (KpcDataSerializer *data_ser, uint8_t index) {
    if(!is_alarm_group_valid(data_ser, index)) {
        return 0;
    }
    return (const KpcAlarmGroup *) (data_ser->buffer + _OFFSET_NTH_ALARM_GROUP(index));
}

void
kpc_data_ser_set_alarm_group (KpcDataSerializer *data_ser, uint8_t index,
                              const KpcAlarmGroup * alarm_group) {
    memcpy(data_ser->buffer + _OFFSET_NTH_ALARM_GROUP(index), alarm_group, sizeof(KpcAlarmGroup));
    set_alarm_group_valid(data_ser, index, true);
}

void
kpc_data_ser_del_alarm_group (KpcDataSerializer *data_ser, uint8_t index) {
    kpc_data_ser_set_enabled_alarm_group(data_ser, index, false);
    set_alarm_group_valid(data_ser, index, false);
}

bool
kpc_data_ser_is_enabled_alarm_group (KpcDataSerializer *data_ser, uint8_t index) {
    if (kpc_data_ser_get_alarm_group(data_ser, index) == 0) {
        return false;
    }
    return get_bitfield(data_ser->buffer+_OFFSET_ALARM_GROUPS_IS_ENABLED_BITFLD, index);
}

void
kpc_data_ser_set_enabled_alarm_group (KpcDataSerializer *data_ser, uint8_t index, bool is_enabled) {
    set_bitfield(data_ser->buffer+_OFFSET_ALARM_GROUPS_IS_ENABLED_BITFLD, index, is_enabled);
}
