#ifndef KPCDATACHANNELCONTROLLER_H_OCMR74UI
#define KPCDATACHANNELCONTROLLER_H_OCMR74UI

#include "KpcDataChannel.h"

#define _KPC_DATA_TRANSPORT_MAGIC_WORD 0xABABCDCD

/* Provides integrity and security pin protection for data transfer
 * through a `KpcDataChannel`.
 *
 * Incoming Data Frame:
 *  ---------------------------------------------------------------
 * | Magic word | Security Pin | Command | Data         | Checksum |
 * | 4 bytes    | 2 bytes      | 1 byte  | (as per cmd) | 1 byte   |
 *  ---------------------------------------------------------------
 *
 * Outgoing Data frame:
 *  ---------------------------------------------------
 * | Magic word | Reply  | Data             | Checksum |
 * | 4 bytes    | 1 byte | (as per reply)   | 1 byte   |
 *  ---------------------------------------------------
 * See enums KPC_DATA_TRANSPORT_CMD and KPC_DATA_TRANSPORT_REPLY for valid values of Command and Reply.
 *
 * Magic Word   : See #define.
 * Security pin : Provided as function argument.
 * Checksum     : Byte by byte XOR of everything else in the frame. */

struct _KpcDataTransport {
    KpcDataChannel *data_channel;
    uint8_t        *push_data;
    uint16_t        push_data_len;
};

typedef struct _KpcDataTransport KpcDataTransport;

enum _KPC_DATA_TRANSPORT_READ_STATUS {
    KPC_DATA_TRANSPORT_READ_STATUS_NO_DATA,        // Nothing there in the channel.
    KPC_DATA_TRANSPORT_READ_STATUS_INVLD_DATA,     // Data was read which didn't match the frame format.
    KPC_DATA_TRANSPORT_READ_STATUS_TIMEOUT,        // Read timeout before completing the frame.
    KPC_DATA_TRANSPORT_READ_STATUS_PIN_ERROR,      // Security pin mismatch.
    KPC_DATA_TRANSPORT_READ_STATUS_CHECKSUM_ERROR, // Checksum mismatch for received data.
    KPC_DATA_TRANSPORT_READ_STATUS_OK,             // Data has been read successfully and is available
                                                   //    for processing by the caller.
};
typedef enum _KPC_DATA_TRANSPORT_READ_STATUS KPC_DATA_TRANSPORT_READ_STATUS;

enum _KPC_DATA_TRANSPORT_CMD {
    KPC_DATA_TRANSPORT_CMD_PUSH = 0x01,
    KPC_DATA_TRANSPORT_CMD_PULL = 0x02,
    KPC_DATA_TRANSPORT_CMD_TEST_ALARM = 0x03,
};
typedef enum _KPC_DATA_TRANSPORT_CMD KPC_DATA_TRANSPORT_CMD;

enum _KPC_DATA_TRANSPORT_REPLY {
    KPC_DATA_TRANSPORT_REPLY_PUSH_OK    = 0x01,
    KPC_DATA_TRANSPORT_REPLY_PULL_OK    = 0x02,
    KPC_DATA_TRANSPORT_REPLY_PIN_ERR    = 0x03,
    KPC_DATA_TRANSPORT_REPLY_CHKSUM_ERR = 0x04,
};
typedef enum _KPC_DATA_TRANSPORT_REPLY KPC_DATA_TRANSPORT_REPLY;

struct _KpcDataTransportRead {
    const KPC_DATA_TRANSPORT_READ_STATUS status;
    const KPC_DATA_TRANSPORT_CMD         cmd; /* valid only if READ_STATUS_OK */
    const union {
        uint8_t                   *push_data;               /* if CMD_PUSH */
        uint8_t                    index_of_alarm_to_test;  /* if CMD_TEST_ALARM */
    };
};
typedef struct _KpcDataTransportRead KpcDataTransportRead;

/* Initialize the Data Transport.
 * prealloc_push_data_buf: A preallocated buffer to which the data is to be read when CMD_PUSH.
 * push_data_buf_len     : The length of the push_data_buf.
 *                         This much bytes are expected as data for CMD_PUSH.
 *                         Length mismatch will result in READ_STATUS_CHECKSUM_ERROR or
 *                         READ_STATUS_TIMEOUT. */
KpcDataTransport                   kpc_data_transport_init (KpcDataChannel *data_channel,
                                                            uint8_t  *prealloc_push_data_buf,
                                                            uint16_t  push_data_buf_len);

/* Reads data from the channel and tries to find the incoming frame described above.
 * `uint8_t *push_data` will be overwritten when CMD_PUSH is received and security_pin matches.
 * ** IMPORTANT **: It will get overwritten regardless of CHECKSUM_ERROR or TIMEOUT.
 * PUSH_OK, PIN_ERR and CHKSUM_ERR reply will be sent automatically (when that is the case). */
KpcDataTransportRead               kpc_data_transport_read_cmd      (KpcDataTransport *data_transport,
                                                                     uint16_t security_pin);
/* Encapsulate the given data in a frame and send it.
 * `data` must of length push_data_buf_len.*/
void                               kpc_data_transport_reply_to_pull (KpcDataTransport *data_transport,
                                                                     uint8_t *data);

#endif /* end of include guard: KPCDATACHANNELCONTROLLER_H_OCMR74UI */
