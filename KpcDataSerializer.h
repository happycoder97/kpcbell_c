#ifndef KPCDATASERIALIZER_H_OJNLVGPU
#define KPCDATASERIALIZER_H_OJNLVGPU

/**
 * KpcDataSerializer
 * Serialize data for sending to bluetooth and storing to EEPROM.
 *
 * Create a fixed size byte array.
 * The functions store data into this buffer.
 * Access the buffer directly from the struct to store in EEPROM or
 * transmit via bluetooth.
 * Do not modify the buffer directly, use the functions instead.
 * The data stored can be parsed back using the get functions.
 *
 * Usage:
 *   - Allocate an instance of KpcDataSerializer on stack, heap or as a global.
 *     (Stack is preferred in embedded devices.)
 *   - Call clear_buffer on that instance.
 *   - Call the set_* functions to set data.
 *   - Use KpcDaaSerializer->buffer to store it to EEPROM or transmit through bluetooth.
 *
 *
 * Internal structure:
 * ---------------------------------------------------------------
 *  Offset  Name                           Size
 * ---------------------------------------------------------------
 *  0       Security Pin                   2  bytes
 *  2       Settings Flags                 1  byte
 *  3       Bell warm up time              1  byte
 *  4       Time Now HH MM                 2  bytes
 *  6       Next Alarm HH MM               2  bytes
 *  8       Alarms data                    72 * 5  bytes = 360 bytes
 *  368     Bitfield: is nth alarm valid   72/ 8 bits    = 9 bytes
 *  377     Alarm Groups data (6 Nos)      6  * 16 bytes = 96  bytes
 *  473     Bitfield: is nth group valid   1  byte
 *  474     Bitfield: is nth group enabled 1  byte
 *
 *  Total: 475 bytes
 *
 *          (nth bit is counted taking LSB = 0th bit)
 *
 *  Note:
 *  The fields Time Now and Next Alarm are not needed
 *  when storing to EEPROM, as the time is obtained from RTC,
 *  and next alarm is dynamically recalculated from alarm data.
 *  But for the sake of simplicity, we ignore that and preserve the same structure
 *  when storing to EEPROM. We simply do not set or read those data
 *  when storing to or reading from EEPROM.
 */

#include "KpcTime.h"
#include <stdbool.h>
#include <stdint.h>

#define KPC_DATA_SER_BUFFER_LEN     475
#define KPC_DATA_SER_N_ALARM_GROUPS 6
#define KPC_DATA_SER_N_ALARMS       72


enum CONSTANTS {
    _OFFSET_SECURITY_PIN                    = 0,
    _OFFSET_SETTINGS_FLAGS                  = 2,
    _OFFSET_BELL_WARM_UP_TIME               = 3,
    _OFFSET_TIME_NOW                        = 4,
    _OFFSET_NEXT_ALARM                      = 6,
    _OFFSET_ALARMS_DATA                     = 8,
    _OFFSET_ALARMS_IS_VALID_BITFLD          = _OFFSET_ALARMS_DATA +
                                              sizeof(KpcAlarm) * KPC_DATA_SER_N_ALARMS,
    _OFFSET_ALARM_GROUPS_DATA               = _OFFSET_ALARMS_IS_VALID_BITFLD +
                                              KPC_DATA_SER_N_ALARMS/8 + (KPC_DATA_SER_N_ALARMS % 8?1:0),
    _OFFSET_ALARM_GROUPS_IS_VALID_BITFLD    = _OFFSET_ALARM_GROUPS_DATA+
                                              sizeof(KpcAlarmGroup)*KPC_DATA_SER_N_ALARM_GROUPS,
    _OFFSET_ALARM_GROUPS_IS_ENABLED_BITFLD  = _OFFSET_ALARM_GROUPS_IS_VALID_BITFLD + 1
};

struct _KpcDataSerializer {
    uint8_t     buffer[KPC_DATA_SER_BUFFER_LEN];
};
typedef struct _KpcDataSerializer KpcDataSerializer;

enum _KPC_SETTINGS_FLAGS {
    KPC_SETTINGS_IS_ALARM_ON    = 0x1,  /* Silent, if not ON */
    KPC_SETTINGS_IS_MP3_ON      = 0x2   /* Uses MP3 if ON. The relay is switched on regardless of this. */
};
typedef enum _KPC_SETTINGS_FLAGS KPC_SETTINGS_FLAGS;

/**
 * Reset buffer to ones, clearing all data
 * Ones, because KPC_INVALID_ALARM and KPC_INVALID_ALARM_GROUP
 * are represented by ones.
 */
void        kpc_data_ser_reset_buffer               (KpcDataSerializer *data_ser);
void        kpc_data_ser_set_time_now               (KpcDataSerializer *data_ser, KpcTime  *time);
void        kpc_data_ser_set_next_alarm             (KpcDataSerializer *data_ser, KpcAlarm *alarm);

void        kpc_data_ser_set_bell_warm_up_time      (KpcDataSerializer *data_ser, uint8_t  warm_up_secs);
void        kpc_data_ser_set_security_pin           (KpcDataSerializer *data_ser, uint16_t sec_pin);
void        kpc_data_ser_set_settings_flag          (KpcDataSerializer *data_ser, uint8_t  flag);
void        kpc_data_ser_clear_settings_flag        (KpcDataSerializer *data_ser, uint8_t  flag);

uint8_t     kpc_data_ser_get_bell_warm_up_time      (KpcDataSerializer *data_ser);
uint16_t    kpc_data_ser_get_security_pin           (KpcDataSerializer *data_ser);
/* bool */
uint8_t     kpc_data_ser_is_settings_flag_set       (KpcDataSerializer *data_ser, uint8_t flag);

/** Obtain a pointer to the alarm at given index.
 *  Returns NUL when:
 *      - No alarm has been previously set at index.
 *      - index >= KPC_DATA_SER_N_ALARMS
 */
const KpcAlarm *  kpc_data_ser_get_alarm   (KpcDataSerializer *data_ser, uint8_t index);
void              kpc_data_ser_set_alarm   (KpcDataSerializer *data_ser, uint8_t index,
                                            const KpcAlarm * alarm);
void              kpc_data_ser_del_alarm   (KpcDataSerializer *data_ser, uint8_t index);


/** Obtain a pointer to the alarm group at given index.
 *  Returns NUL when:
 *      - There is no alarm grou at given index.
 *      - index >= KPC_DATA_SER_N_ALARMS
 */
const KpcAlarmGroup * kpc_data_ser_get_alarm_group        (KpcDataSerializer *data_ser, uint8_t index);
void                  kpc_data_ser_set_alarm_group        (KpcDataSerializer *data_ser, uint8_t index,
                                                           const KpcAlarmGroup * alarm_group);
void                  kpc_data_ser_del_alarm_group        (KpcDataSerializer *data_ser, uint8_t index);
bool                  kpc_data_ser_is_enabled_alarm_group (KpcDataSerializer *data_ser, uint8_t index);
void                  kpc_data_ser_set_enabled_alarm_group(KpcDataSerializer *data_ser, uint8_t index,
                                                           bool is_enabled);

#endif /* end of include guard: KPCDATASERIALIZER_H_OJNLVGPU */
