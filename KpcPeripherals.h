#ifndef KPCPERIPHERALS_H_J9HDWP6R
#define KPCPERIPHERALS_H_J9HDWP6R

/* This module intends to isolate the platform and hardware specific initialization
 * of different peripherals, so that they can be swapped out easily for porting to
 * different hardware or for unit testing.
 *
 * For the sake of simplicity let me use `KpcSoundPlayer` as an example:
 * `KpcSoundPlayer` may be implemented for the DFPlayer module or direct PWM audio output by reading
 * wav files from sd cards.
 * `KpcSoundPlayerDFPlayer` needs only two serial ports, but `KpcSoundPlayerPwmSd` needs
 * SPI for sd card and PWM output pin for audio output.
 * Clearly `kpc_snd_player_init(..)` cannot have a uniform interface.
 *
 * So `KpcSoundPlayerDFPlayer.c` should implement the function
 *          `KpcSoundPlayerDFPlayer* kpc_snd_player_df_player_init(..)`
 * which should be defined in `KpcSoundPlayerDFPlayer.h`.
 * Ditto for `KpcSoundPlayerPwmSd`.
 *
 * Then the implementation of `kpc_peripherals_init()` calls the init function for the chosen
 * hardware.
 */


#include "KpcDataChannel.h"
#include "KpcClock.h"
#include "KpcRelayBell.h"
#include "KpcSoundPlayer.h"

struct _KpcPeripherals {
    KpcDataChannel  *data_channel;
    KpcClock        *clock;
    KpcRelayBell    *relay_bell;
    KpcSoundPlayer  *snd_player;
};
typedef struct _KpcPeripherals KpcPeripherals;

KpcPeripherals  kpc_peripherals_init();

#endif /* end of include guard: KPCPERIPHERALS_H_J9HDWP6R */
