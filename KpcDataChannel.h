#ifndef KPCDATACHANNEL_H_8HN257WS
#define KPCDATACHANNEL_H_8HN257WS

#include <stdbool.h>
#include <stdint.h>

/* Abstract away the underlying data transfer channel
 * between the microcontroller and the controlling app.
 * Eg: Bluetooth, Wifi, etc.
 */


struct _KpcDataChannel;
typedef struct _KpcDataChannel KpcDataChannel;

bool        kpc_data_channl_is_connected(KpcDataChannel *data_channel);

uint8_t     kpc_data_channel_read_byte();
bool        kpc_data_channel_did_last_read_succeed();


#endif /* end of include guard: KPCDATACHANNEL_H_8HN257WS */
