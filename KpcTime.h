#ifndef __ALARM_H__
#define __ALARM_H__

#include <stdint.h>

struct _KpcTime {
    uint8_t hour_24;
    uint8_t minute;
};

struct _KpcAlarm {
    uint8_t hour_24;
    uint8_t minute;
    uint8_t duration_secs;
    uint8_t sound_id;   /* Ignored if not using sound playback. */
    uint8_t group_id;   /* Index of alarm*/
};

struct _KpcAlarmGroup {
    /* bit 0(LSB) to bit 6: SMTWTFS. MSB is ignored */
    uint8_t active_weekdays;
    uint8_t name[15];
};


typedef struct _KpcTime  KpcTime;
typedef struct _KpcAlarm KpcAlarm;
typedef struct _KpcAlarmGroup KpcAlarmGroup;

#endif /* end of include guard: __ALARM_H__ */
